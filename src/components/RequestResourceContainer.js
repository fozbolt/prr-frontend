import React from 'react'
import RequestResourceCard from './RequestResourceCard.js'

function RequestResourcesContainer({data}) {
  const [prrData, setPrrData] = React.useState(data)

  React.useEffect(() => {
    if (data) {
      setPrrData(data);
    }
  }, [data]);

 
  
  const styles = {

    resourcesContainer: {
      width: '95%',
      minHeight: '100px',
      borderStyle: 'dashed',
      borderColor: '#E2E2E2',
      backgroundColor: 'white',
      margin: '5px 10px 5px 30px',
      display: 'flex',
    },
  };


  return (
    <div style={styles.resourcesContainer}>
             <RequestResourceCard data={prrData} /*cardCallback={handleCardCallback}*/ /> 
    </div>
  )
}

export default RequestResourcesContainer
